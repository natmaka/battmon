 # Description

This (very Linux-specific and crude) bash script monitors the computer battery and warns (low charge, AC status changed: connected/disconnected) by messaging each tty (terminal, shell session) and also opening a X Window popup.

Status: very new tool, test in progress.

# Installation

## Install mandatory tools

Install touch (it probably is already installed), wall, logger and zenity.

## Copy the scripts

Copy the provided *battmon* script and also *bash_nat_std* (from the natmaka/nat_script> project) in a directory (probably under your home directory).
If you already have *bash_nat_std* somewhere do not copy it, just create a symbolic link to it.

## Set parameters

Edit battmon:
* mandatory_first_ac_status: mandatory status of the AC adapter upon startup of the *battmon* script: '' (empty string) if you don't care, 0 if OFF, 1 if ON 
* batt_min_cap: battmon will alert you if the battery capacity falls below this percentage
* seconds_btw_alerts: min amount of seconds between two consecutive alerts

Do not modify the *beacon* parameter.

## Test

Invoke battmon and let it run for a while.  Plug/unplug your AC adapter, it should emit a warning.
Modify its parameters and restart it to test whether it detects some condition.

## Have it run automagically

Invoke battmon at session start time.

In many context it can be done by adding a line in *~/.xsession*:
``` shell
~/**mytool**/battmon & # replace _mytool_ by the name of the directory where battmon resides
```
