#!/bin/bash
# Time-stamp: <2023-07-14 15:07:52 nat>

mandatory_first_ac_status=1
batt_min_cap=80
seconds_btw_alerts=36000
beacon=/tmp/battmon.beacon
declare -r mandatory_first_ac_status batt_min_cap seconds_btw_alerts beacon

MYDIR=$(dirname -- "$( readlink -f -- "$0"; )")
source "${MYDIR}/bash_nat_std" || exit 4

[[ -z "$DISPLAY" ]] && die 'I am only useful in a X Window session'

check_installed() {
    [ -x "$(command -v "$1")" ] || die "Please install $1"
}

alert() {
    if [[ -e "$beacon" ]] ; then
        (( EPOCHSECONDS - $(stat -c "%Y" $beacon) < seconds_btw_alerts )) && return
    fi
    touch "$beacon"
    wall -t 5 battmon: "$@" &
    logger -t battmon "$@" &
    zenity --warning --text="<span font=\"48\" foreground=\"red\" weight=\"heavy\">battmon\n$*</span>" --title=battmon --timeout "$seconds_btw_alerts"
}

match_regexp() {
    [[ "$1" =~ $2 ]] && echo "${BASH_REMATCH[1]}" || echo ''
}

check_installed upower
check_installed wall
check_installed logger
check_installed zenity
check_installed touch

if [[ -n "$mandatory_first_ac_status" ]] ; then
    ((mandatory_first_ac_status<0 || mandatory_first_ac_status>1)) && die "Parameter mandatory_first_ac_status badly set"
fi

((batt_min_cap<1)) || ((batt_min_cap>100)) && die "batt_min_cap is a percentage"

POWER_ADAPTER="/sys/class/power_supply/ADP$(match_regexp "$(upower -e|grep -E '^/.*_ADP[0-9]+$')" "^/org/freedesktop/UPower/devices/line_power_ADP([0-9]+)$")/online"
unset freedesktop_POWER_BAT freedesktop_POWER_ADAPTER dev_BAT dev_ADAPTER
POWER_BAT="/sys/class/power_supply/BAT$(match_regexp "$(upower -e|grep -E '^/.*_BAT[0-9]+$')" "^/org/freedesktop/UPower/devices/battery_BAT([0-9]+)$")/capacity"
declare -r POWER_BAT POWER_ADAPTER

[[ -r "$POWER_BAT" ]] || die "Unreadable POWER_BAT ($POWER_BAT), does this computer have a battery?"
[[ -r "$POWER_ADAPTER" ]] || die "Unreadable POWER_ADAPTER ($POWER_ADAPTER)"

[[ -n "$mandatory_first_ac_status" ]] && PREVIOUS_AC_ADAPTER="$mandatory_first_ac_status" || PREVIOUS_AC_ADAPTER=$(< "$POWER_ADAPTER")

while :
do
    AC_ADAPTER=$(< "$POWER_ADAPTER")
    if (( AC_ADAPTER != PREVIOUS_AC_ADAPTER )) ; then
        alert "new AC power state: $AC_ADAPTER"
        PREVIOUS_AC_ADAPTER=$AC_ADAPTER
    fi
    (( $(< "$POWER_BAT") < batt_min_cap )) && alert "battery low"
    sleep 1m
done
